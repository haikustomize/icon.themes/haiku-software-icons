# Haiku Software Icons

Icons for available or soon available software for the Haiku operating system according to the default icon theme.


These icons are all in Icon-O-Matic format.

LibreOffice icon is the new revision for the LibreOffice software. 
